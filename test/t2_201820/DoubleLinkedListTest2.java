package t2_201820;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;
import model.vo.VOStations;

public class DoubleLinkedListTest2 {

	
	
	private DoubleLinkedList lista;
	private VOStations nuevo= new VOStations(20, "aaa", "Bogota", 20.1, 50.3, 2, "2017");
	private VOStations nuevo2= new VOStations(40, "bbb", "Chia", 21.2, 100, 5, "2019");
	private 	VOStations nuevo3= new VOStations(50, "ccc", "Djajd", 15, 0, 3, "2016");
	private Node uno=new Node(nuevo);
	private Node dos=new Node(nuevo2);
	private Node tres=new Node(nuevo3);
	 @Before
	 public void setupEscenario1( )
	 {
	     lista = new DoubleLinkedList<>();   
	     lista.add(tres);
	 }
	 
	 @Test
	 public void testAdd() {
		 
		lista.add(uno);
	
		assertTrue("debio agregarse el objeto", lista.getElement(0)==uno);
	     
	 }
	 
	 @Test
	 public void testGetSize() {
		 setupEscenario1();
		 System.out.println(lista.getSize());
		 assertTrue("Deberia ser 1", lista.getSize()==1);
	 }
	 
	 @Test
	 public void testAddAtEnd() {
		 lista.addAtEnd(dos);
		 assertTrue("deberia ser bbb",lista.getElement(1)==dos );
	 }
	 /*
	 @Test
	 public void testAddAtK() {
		 lista.AddAtK(dos, 1);
		 System.out.println(lista.getElement(1));
		 assertTrue("Deberia ser", lista.getElement(1)==(dos));
	 }
	 */
	 
	 @Test 
	 public void testGetCurrentElement() {
		 assertTrue("Deberia ser el 1", lista.getCurrentElement()==tres);
	 }
	 @Test
	 public void testGetElement() {
		 assertTrue(lista.getElement(0)==tres);
	 }
	 @Test
	 public void testDelete() {
		 lista.addAtEnd(dos);
		 lista.addAtEnd(uno);
		 lista.delete(tres);
		 assertTrue(lista.getElement(0)==dos);
	 }
	 
	
	
}
