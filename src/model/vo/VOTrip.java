package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip> {

	private int Id;
	private String Start;
	private String End;
	private int BikeId;
	private double Duration; 
	private int FromId;
	private String FromName;
	private int ToId;
	private String ToName;
	private String User;
	private String Gender;
	private String Birth;
	
	public VOTrip (int pId, String pStart, String pEnd, int pBikeId, double pDuration, int pFromId, String pFromName, int pToId, String pToName, String pUser, String pGender, String pBirth) {
		Id=pId;
		Start= pStart;
		End= pEnd;
		BikeId= pBikeId;
		Duration=pDuration;
		FromId=pFromId;
		FromName=pFromName;
		ToId=pToId;
		ToName=pToName;
		User=pUser;
		Gender=pGender;
		Birth=pBirth;
		
		
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return Id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return Duration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return FromName;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return ToName;
	}

	
public int darId() {
	return Id;
}

public String darGender() {
	return Gender;
}
public int darToId() {
	return ToId;
}
	@Override
	public int compareTo(VOTrip o) {
		// TODO Auto-generated method stub
		return Id-o.darId();
	}
}
