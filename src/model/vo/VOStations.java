package model.vo;

public class VOStations implements Comparable<VOStations> {
	private int Id;
	private String name;
	private String city;
	private double lat;
	private double longi;
	private int capacity;
	private String online;

	public VOStations(int pId, String pName, String pCity, double pLat, double pLongi, int pCapacity, String pOnline) {
		Id=pId;
		name=pName;
		city=pCity;
		lat=pLat;
		longi=pLongi;
		capacity=pCapacity;
		online=pOnline;
	}
	@Override
	public int compareTo(VOStations o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public String darNombre() {
		return name;
	}
	public int darId() {
		return Id;
	}
	
	
	

}
