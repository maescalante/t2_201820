package model.logic;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;

import com.opencsv.CSVReader;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import api.IDivvyTripsManager;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {

private DoubleLinkedList estaciones;
private DoubleLinkedList viajes;

	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		estaciones=new DoubleLinkedList<>();
		CSVReader reader=null;
	     try {
	    	 reader = new CSVReader(new FileReader(new File(stationsFile)));
		     String [] nextLine;
		    int i=0;
			while ((nextLine = reader.readNext()) != null) {
			    // nextLine[] is an array of values from the line
				
				if (i>0) {
				VOStations temp= new VOStations(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6]);
				Node a= new Node(temp);
				
				estaciones.add(a);
				}
				i++;
			 }
			
			 reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		viajes=new DoubleLinkedList<>();
		CSVReader reader=null;
	     try {
	    	 reader = new CSVReader(new FileReader(new File(tripsFile)));
		     String [] nextLine;
		    int i=0;
		   
			while ((nextLine = reader.readNext()) != null) {
			    // nextLine[] is an array of values from the line
				
				if (nextLine[10]=="") {
					
					nextLine[10]=null;
					nextLine[11]=null;
				}
				if (i>0) {
				VOTrip temp= new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], Integer.parseInt(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), nextLine[8], nextLine[9], nextLine[10], nextLine[11]);
				Node a= new Node(temp);
				
				viajes.add(a);
				}
				i++;
				
			
			 }
			
			 reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public IDoubleLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		Node<VOTrip> a=viajes.getCurrentElement();
		DoubleLinkedList<VOTrip> ret=new DoubleLinkedList<VOTrip>();
	
		while (a!=null) {
			if (a.darObjeto().darGender().equals(gender) && a.darObjeto().darGender()!=null) {
				Node<VOTrip> b= new Node<VOTrip>(a.darObjeto());
				ret.add(b);
		
			}
			
			a=a.darSiguiente();
		
		}
	
		return ret;
	}

	@Override
	public IDoubleLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		Node<VOTrip> a=viajes.getCurrentElement();
		DoubleLinkedList<VOTrip> ret=new DoubleLinkedList<VOTrip>();
		
		while (a!=null) {
			if (a.darObjeto().darToId()==stationID) {
				Node<VOTrip> b= new Node<VOTrip>(a.darObjeto());
				ret.add(b);
			}
			a=a.darSiguiente();
		}
		return ret;
	}	


}
