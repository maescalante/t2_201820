package model.data_structures;

import java.util.Iterator;

import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;


public class DoubleLinkedList<T extends Comparable<T>> implements IDoubleLinkedList<T> {
	private Node primerNodo;
	
	public  DoubleLinkedList() {
		primerNodo=null;
	}
	
	
	private class ListIterator implements Iterator<T>{
		private Node<T> current= primerNodo;
		public boolean hasNext() {
			return current !=null;
		}
		public void remove() {

		}
		public T next() {
			T item=  current.darObjeto();
			current=current.darSiguiente();
			return item;
		}
	}
	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}

	@Override
	public Integer getSize() {
		int cont=0;
		Node actual=primerNodo;
		while (actual!=null) {
			cont++;
			actual=actual.darSiguiente();
		}
		return cont;
	}

	@Override
	public void add(Node pNodo) {
		if (primerNodo==null) {
			primerNodo=pNodo;
		}else {
			Node temp=primerNodo;
			primerNodo=pNodo;
			primerNodo.cambiarSiguiente(temp);
			
			
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAtEnd(Node pNodo) {
		// TODO Auto-generated method stub
		Node actual=primerNodo;
		while (actual.darSiguiente()!=null) {
			actual=actual.darSiguiente();
		}
		actual.cambiarSiguiente(pNodo);
	}

	@Override
	public void AddAtK(Node pNodo, int k) {
		// TODO Auto-generated method stub
		int i=0;
		Node actual=primerNodo;
		while (i!=k && actual.darSiguiente()!=null) {
			actual=actual.darSiguiente();
			i++;
		}
		Node temp=actual;
	
		actual= pNodo;
		pNodo.cambiarAnterior(temp.darAnterior());
		pNodo.cambiarSiguiente(temp);
		

	}

	@Override
	public Node getElement(int k) {
		Node actual= primerNodo;
		int i=0;
		while (i!=k && actual.darSiguiente()!=null) {
			actual=actual.darSiguiente();
			i++;
		}
		return actual;
		// TODO Auto-generated method stub
		
	}

	@Override
	public Node getCurrentElement() {
		// TODO Auto-generated method stub
		return primerNodo;
	}

	@Override
	public void delete(Node pNode) {
		Node actual =primerNodo;
		boolean flag=false;
		if (actual==pNode) {
			Node temp=primerNodo.darSiguiente();
			primerNodo=temp;
		}else {
		while (actual.darSiguiente()!=null && flag==false) {
			if (actual==pNode) {
				actual.cambiarAnterior(actual.darSiguiente());
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
				flag=true;
			}
			actual=actual.darSiguiente();
		}
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAtK(Node pNode ,int k) {
		Node actual =primerNodo;
		boolean flag=false;
		int i=0;
		while (actual.darSiguiente()!=null && flag==false) {
			if (i==k) {
				actual.cambiarAnterior(actual.darSiguiente());
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
				flag=true;
			}
			i++;
			actual=actual.darSiguiente();
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public void next() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void previous() {
		// TODO Auto-generated method stub
		
	}

}
