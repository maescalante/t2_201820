package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoubleLinkedList<T extends Comparable<T>> extends Iterable<T> {

	Integer getSize();
	
	public void add(Node pNodo);
	
	public void addAtEnd(Node pNodo);
	
	public void AddAtK(Node pNodo, int k);
	
	public Node getElement(int k);
	
	public Node getCurrentElement();

	public void delete(Node pNode);
	
	public void deleteAtK(Node pNode, int k);
	
	public void next();
	
	public void previous();
}
