package model.data_structures;

public class Node <T extends Comparable<T>>  {

	private Node<T> anterior;
	private Node<T> siguiente;
	private T Objeto;
	public  Node (T pObjeto) {
		Objeto=pObjeto;
		anterior=null;
		siguiente=null;
	}
	
	public Node darSiguiente() {
		return siguiente;
	}
	
	public Node darAnterior() {
		return anterior;
		
	}
	
	public void cambiarSiguiente(Node pSiguiente) {
		siguiente=pSiguiente;
	}
	
	public void cambiarAnterior(Node pAnterior) {
		siguiente=pAnterior;
	}
	public T darObjeto() {
		return Objeto;
	}
	
	
	
}
